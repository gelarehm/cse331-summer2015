package assignments.assignment1;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by jessehartloff on 5/11/15.
 */
public class Tests {

    // These tests are for the correctness of the algorithms implemented, not for runtime.
    // Also, these tests do not guarantee that your algorithms are correct and should mostly be used to ensure that
    // you are using the conventions that I am expecting during grading.

    @Test
    public void TestPolynomialEvaluation0(){
        int[] coefficients = new int[] {0};
        int evalPoint = 7;

        int computed = Assignment1.polynomialEvaluation(coefficients, evalPoint);
        int expected = 0;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, computed == expected);
    }

    @Test
    public void TestPolynomialEvaluation1(){
        int[] coefficients = new int[] {2,3,4};
        int evalPoint = 5;

        int computed = Assignment1.polynomialEvaluation(coefficients, evalPoint);
        int expected = 117;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, computed == expected);
    }

    @Test
    public void TestPolynomialEvaluation2(){
        int[] coefficients = new int[] {10,9,8,7,6,5,4,3,2,1,0};
        int evalPoint = 1;

        int computed = Assignment1.polynomialEvaluation(coefficients, evalPoint);
        int expected = 55;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, computed == expected);
    }


    @Test
    public void TestExponentiation0(){
        int base = 100;
        int exponent = 0;

        int computed = Assignment1.exponentiation(base, exponent);
        int expected = 1;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, computed == expected);
    }

    @Test
    public void TestExponentiation1(){
        int base = 1;
        int exponent = 7;

        int computed = Assignment1.exponentiation(base, exponent);
        int expected = 1;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, computed == expected);
    }

    @Test
    public void TestExponentiation2(){
        int base = 5;
        int exponent = 10;

        int computed = Assignment1.exponentiation(base, exponent);
        int expected = 9765625;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, computed == expected);
    }

}
