package assignments.assignment1;

/**
 * Created on 5/11/15.
 */
public class Assignment1 {

    /***
     * Performs integer polynomial evaluation of a polynomial defined by coefficients
     *  evaluated at x.
     *
     * @param coefficients the coefficients of the polynomial to be evaluated indexed by exponent.
     *                     (ex. coefficients[0] is the constant term and coefficients[i] is the
     *                     coefficient for the x^i term)
     * @param x the value at which the polynomial is to be evaluated.
     * @return the evaluation of the polynomial defined by coefficients evaluated at x
     */
    public static int polynomialEvaluation(int[] coefficients, int x){
        //TODO: complete this function in O(d) time where d is the degree of the
        // polynomial (ie. linear in coefficients.length)
        return 0;
    }


    /***
     * Performs integer exponentiation.
     *
     * @param a base
     * @param b exponent
     * @return a^b
     */
    public static int exponentiation(int a, int b){
        //TODO: perform integer exponentiation in O(log(b)) time
        // Note that the runtime should be logarithmic in b so it will be linear in
        // terms of the size of b in bits
        return 0;
    }

}
