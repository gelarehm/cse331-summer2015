package assignments.assignment4;

import java.util.Arrays;

/**
 * Created by jessehartloff on 7/19/15.
 */
public class Main {

    public static void main(String[] args) {
        int n = 20;
        SpaceElevator elevator = new SpaceElevator(n, new NuclearFuel());
        System.out.println(elevator);

        boolean[] schedule = Assignment4.generateSchedule(elevator);
        double cost = Assignment4.computeCost(elevator, schedule);

        System.out.println();
        System.out.println("schedule: " + Arrays.toString(schedule));
        System.out.println("cost: " + cost);
    }

}
