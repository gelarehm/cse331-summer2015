package assignments.assignment4;

/**
 * Created by jessehartloff on 7/19/15.
 */
public interface AlternateFuel {

    int NUCLEAR = 0;
    int OIL = 1;
    int STEAM = 2;

    double cost(int remainingDistance);

    int getID();

}
