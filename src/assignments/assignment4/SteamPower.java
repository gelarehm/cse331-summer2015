package assignments.assignment4;

/**
 * Created by jessehartloff on 7/19/15.
 */
public class SteamPower implements AlternateFuel {

    @Override
    public double cost(int remainingDistance) {
        return 50.0*Math.pow(remainingDistance, 3.0);
    }

    @Override
    public int getID() {
        return AlternateFuel.STEAM;
    }

    @Override
    public String toString() {
        return "Steam Power";
    }
}
