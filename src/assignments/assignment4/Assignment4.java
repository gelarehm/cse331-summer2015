package assignments.assignment4;

import java.util.List;

/**
 * Created on 7/1/15.
 */
public class Assignment4 {

    /**
     * computes a schedule as an array of booleans. The array is true at index i if the schedule is to complete the
     * current shipment with the alternate fuel after using dark matter i. The schedule should minimize the alternate
     * fuel cost.
     */
    public static boolean[] generateSchedule(SpaceElevator elevator){
        List<Integer> upcomingDarkMatter = elevator.getUpcomingDarkMatterDrops();
        boolean[] schedule = new boolean[upcomingDarkMatter.size()];
        // TODO
        return schedule;
    }

    /**
     * computes the alternate fuel cost for a given elevator and schedule.
     */
    public static double computeCost(SpaceElevator elevator, boolean[] schedule){
        // TODO
        return Double.POSITIVE_INFINITY;
    }

}
