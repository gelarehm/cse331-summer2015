package assignments.assignment2;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;

/**
 * Created by jessehartloff on 6/20/15.
 */
public class Assignment2 {

    /***
     * Generates a schedule of speed runners that minimizes the completion time of all team members. Each speed runner
     * has a known time for the mario portion and the doom 2 portion of the competition. Since each team only has one
     * NES, the mario portion must be performed one at a time and each player must complete the mario portion before
     * beginning the Doom 2 portion. Each team is given as many PC's as they need, so all team members can play Doom 2
     * at the same time. The completion time is the time take for all team member to finish both portions.
     *
     * @param problem The speed runner problem containing a set of all team members along with their Mario
     *                           and Doom 2 times.
     * @return And ordering of all team members in the form of a List that minimizes the completion time for the team
     */
    public static List<SpeedRunner> generateSchedule(TheSpeedRunnerProblem problem){
        // TODO
		return null;
    }
}

