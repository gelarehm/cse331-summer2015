package assignments.assignment2;

import java.util.List;

/**
 * Created by jessehartloff on 6/22/15.
 */
public class Main {

    public static void main(String[] args) {

        int n=10;
        TheSpeedRunnerProblem speedRunnerProblem = new TheSpeedRunnerProblem(n);
        List<SpeedRunner> schedule = Assignment2.generateSchedule(speedRunnerProblem);

        System.out.println();
        System.out.println(schedule);

    }

}
