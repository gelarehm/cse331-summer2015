package assignments.assignment2;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jessehartloff on 6/22/15.
 */
public class TheSpeedRunnerProblem {

    private Set<SpeedRunner> speedRunners;
    private int n;


    public TheSpeedRunnerProblem(int n){
        this.n = n;
        speedRunners = new HashSet<>(n);
        for (int i = 0; i < n; i++) {
            speedRunners.add(new SpeedRunner());
        }
    }


    public Set<SpeedRunner> getSpeedRunners(){
        return speedRunners;
    }

    public int getN(){
        return n;
    }

    @Override
    public String toString() {
        return "speedRunners=" + speedRunners;
    }
}
