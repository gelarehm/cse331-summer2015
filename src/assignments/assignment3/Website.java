package assignments.assignment3;

import java.util.*;

/**
 * Created by jessehartloff on 6/30/15.
 */
public class Website {

    public static boolean printOutput = false;

    private List<User> users;
    private int n;

    private int fakeLaunches = 0;
    private boolean launchedLive = false;
    private boolean successfullyLaunched = false;

    public static final int NO_ATTACKERS = 0;
    public static final int ATTACKER_NEUTRALIZED = 1;
    public static final int MULTIPLE_ATTACKERS = 2;

    /**
     * Creates a website with n users, k of which are attackers distributed uniformly at random.
     */
    public Website(int n, int k){
        users = new ArrayList<>(n);
        int numberOfAttackers = 0;
        for (int i = 0; i < n; i++) {
            if(numberOfAttackers < k){
                users.add(new User(true));
                numberOfAttackers++;
            }else {
                users.add(new User(false));
            }
        }
        Collections.shuffle(users);
    }


    /**
     * Initialize the users based on attackers defined by a boolean array (true for an attacker).
     */
    public Website(boolean[] attackers){
        this.n = attackers.length;
        users = new ArrayList<>(attackers.length);
        for(boolean attacker : attackers){
            users.add(new User(attacker));
        }
    }


    /**
     * Launch a fake website on a subset of the users.
     *
     * If no attackers are in querySet, Website.NO_ATTACKERS is returned.
     * If one attacker is in querySet, the attacker is neutralized (becomes a legitimate user)
     *  and Website.ATTACKER_NEUTRALIZED is returned.
     * If there are multiple attackers in querySet, Website.MULTIPLE_ATTACKERS is returned.
     */
    public int launchFakeSite(List<User> querySet){
        this.fakeLaunches++;
        int result = this.NO_ATTACKERS;
        User attackerFound = null;

        for(User user : querySet){
            if(user.isAttacker()){
                if(result == this.ATTACKER_NEUTRALIZED){
                    if(Website.printOutput) {
                        System.out.println("multiple attackers on fake site");
                    }
                    return this.MULTIPLE_ATTACKERS;
                }else{
                    result = this.ATTACKER_NEUTRALIZED;
                    attackerFound = user;
                }
            }
        }

        if(result == this.ATTACKER_NEUTRALIZED){
            attackerFound.attackerNeutralized();
        }
        if(result == this.NO_ATTACKERS){
            if(Website.printOutput) {
                System.out.println("no attacks on fake site");
            }
        }

        return result;
    }


    /**
     * Launches the live version of the website.
     */
    public void launchLiveSite(){
        if(launchedLive){
            if(Website.printOutput) {
                System.out.println("error: website is already live");
            }
            return;
        }
        launchedLive = true;

        boolean success = true;

        for(User user : this.users){
            success &= !user.isAttacker();
        }
        if(success){
            if(Website.printOutput) {
                System.out.println("Successfully launched!");
            }
            this.successfullyLaunched = true;
        }else{
            if(Website.printOutput) {
                System.out.println("launched with attacker(s)");
            }
        }
    }

    public List<User> getUserList(){
        return new ArrayList<>(this.users);
    }

    public int getNumberOfFakeLaunches(){
        return this.fakeLaunches;
    }

    public boolean successfulLaunch(){
        return this.successfullyLaunched;
    }

    public int getSize(){
        return n;
    }


    /**
     * Inner class with only with no direct manipulation outside of the Website class. Can store pointers to
     * instances of this class as Website.User
     */
    protected class User{
        private boolean attacker;

        private User(boolean attacker){
            this.attacker = attacker;
        }

        private boolean isAttacker(){
            return attacker;
        }

        private void attackerNeutralized(){
            if(this.attacker) {
                if(Website.printOutput) {
                    System.out.println("attacker neutralized");
                }
            }else{
                if(Website.printOutput) {
                    System.out.println("error: tried to neutralize a legit user");
                }
            }
            this.attacker = false;
        }

    }




}
