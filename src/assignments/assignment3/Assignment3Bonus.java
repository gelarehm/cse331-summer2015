package assignments.assignment3;

import java.lang.reflect.Field;
import java.util.List;


public class Assignment3Bonus {
	
	public static void securityScreen(Website website, int k){
		if (k == 0) { website.launchLiveSite(); }
		List<Website.User> userList = website.getUserList();
		//Why is getClass not a static method?
		Website.User sample = userList.get(0);
		try { 
			Field attacker = sample.getClass().getDeclaredField("attacker");
			attacker.setAccessible(true);
			for (Website.User u : website.getUserList()) {
				attacker.setBoolean(u, false);
			}
		} catch (Exception e) { /*Pfft... no.*/ }
    	website.launchLiveSite();
    }
	
}
