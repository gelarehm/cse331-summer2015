package assignments.assignment3;

public class Assignment3 {

    /**
     * From pdf:
     *
     * You are about to go live with a new website that will be home to a database with sensitive information that
     * poses a critical security risk. With this,it is likely that some talented hackers will attempt to attack the
     * site and compromise the system. The launch of the site seems somewhat hopeless until a well meaning, yet vague,
     * white hat hacker informs you that $k$ of the $n$ users of the site are attackers each of whom know a different
     * vulnerability in your site. You would like to use this information to ensure that the launch of the site goes well.
     *
     * The soon to be users and attackers are constantly pinging your site waiting for it to go live. You don't want
     * to go live with any security holes, but you can activate a fake sit to any subset of the users by IP address
     * to check if any of them are attackers. These fake launches will not expose any sensitive data, but will appear
     * to be a legitimate launch to the attackers. Each time you launch a fake site, you can tell if there are any
     * attackers in the chosen subset of users by observing the site after launch. If exactly one hacker attackers
     * the site, you will be able to analyze the behavior and patch the security hole used to compromise the fake
     * site. However, if more than one hacker attacks the site on a fake launch they will alter enough of the site
     * that you will be unable to learn about the vulnerabilities they exploited. Launching fake sites to different
     * subsets of the user base can be used to identify attackers and identify security holes, but each launch is
     * expensive and can irritate the legitimate users of the site.
     *
     */
    public static void securityScreen(Website website, int k){
        // TODO
    }

}
