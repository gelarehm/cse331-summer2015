package assignments.assignment3;

/**
 * Created by jessehartloff on 7/18/15.
 */
public class PA3Grader {


    private final static int MAX_N = 100;
    private final static int MAX_K = 4;


    public static void main(String[] args) {

        int grade = 0;

        for (int k = 1; k <= MAX_K; k++) {

            int worstCaseLaunches = 0;
            for (int n = k+1; n <= MAX_N; n++) {
                boolean[] attackers = new boolean[n];
                int totalLaunches = permute(attackers, n-1, k);
                if(totalLaunches > worstCaseLaunches){
                    worstCaseLaunches = totalLaunches;
                }
            }

            System.out.print(worstCaseLaunches + " worst case fake launch(es) for k=" + k + " and n<=100: ");
            switch(k) {
                case 1:
                    if(worstCaseLaunches <= 1){
                        grade += 20;
                        System.out.println("20 points");
                    }else if(worstCaseLaunches <= 5){
                        grade += 15;
                        System.out.println("15 points");
                    }else if(worstCaseLaunches < Integer.MAX_VALUE){
                        grade += 10;
                        System.out.println("10 points");
                    }else{
                        System.out.println("0 points ");
                    }
                    break;
                case 2:
                    if(worstCaseLaunches <= 14){
                        grade += 30;
                        System.out.println("30 points");
                    }else if(worstCaseLaunches <= 20){
                        grade += 20;
                        System.out.println("20 points");
                    }else if(worstCaseLaunches < Integer.MAX_VALUE){
                        grade += 10;
                        System.out.println("10 points");
                    }else{
                        System.out.println("0 points");
                    }
                    break;
                case 3:
                    if(worstCaseLaunches <= 14){
                        grade += 25;
                        System.out.println("25 points");
                    }else if(worstCaseLaunches <= 20){
                        grade += 15;
                        System.out.println("15 points");
                    }else if(worstCaseLaunches < Integer.MAX_VALUE){
                        grade += 10;
                        System.out.println("10 points");
                    }else{
                        System.out.println("0 points");
                    }
                    break;
                case 4:
                    if(worstCaseLaunches <= 24){
                        grade += 25;
                        System.out.println("25 points");
                    }else if(worstCaseLaunches <= 30){
                        grade += 20;
                        System.out.println("20 points");
                    }else if(worstCaseLaunches < Integer.MAX_VALUE){
                        grade += 10;
                        System.out.println("10 points");
                    }else{
                        System.out.println("0 points");
                    }
                    break;
            }
        }

        System.out.println();
        System.out.println("PA3 grade: " + grade);

    }


    private static int permute(boolean[] attackers, int n, int k) {
        return permute(attackers, n, k, k);
    }


    private static int permute(boolean[] attackers, int n, int k, int totalK){

        if(n+1<k || k<0){
            return 0;
        }

        if(n<0){
            Website site = new Website(attackers);
            Assignment3.securityScreen(site, totalK);
            if(site.successfulLaunch()){
                return site.getNumberOfFakeLaunches();
            }else{
                return Integer.MAX_VALUE;
            }
        }

        attackers[n] = true;
        int launchesWith = permute(attackers, n-1, k-1, totalK);

        attackers[n] = false;
        int launchesWithout = permute(attackers, n-1, k, totalK);

        return Math.max(launchesWith, launchesWithout);
    }


}
