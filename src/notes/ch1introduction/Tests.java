package notes.ch1introduction;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by jessehartloff on 5/11/15.
 */
public class Tests {

    @Test

    public void testBruteForce(){
        StableMarriageProblem problem = new StableMarriageProblem(10);
        StableMatchingAlgorithm bruteForce = new BruteForceStableMarriageRandomized();
        bruteForce.execute(problem);
//        System.out.println(problem);
//        problem.printMatches();
        TestCase.assertTrue("Not stable", problem.isStable());
    }
    @Test
    public void testGaleShapely(){
        StableMarriageProblem problem = new StableMarriageProblem(2000);
        StableMatchingAlgorithm galeShapely = new GaleShapelyAlgorithm();
        galeShapely.execute(problem);
//        System.out.println(problem);
//        problem.printMatches();
        TestCase.assertTrue("Not stable", problem.isStable());
    }

}
