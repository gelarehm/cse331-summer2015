package notes.ch1introduction;

/**
 * Created by jessehartloff on 5/12/15.
 */
public interface StableMatchingAlgorithm {

    public void execute(StableMarriageProblem stableMarriageProblem);

}
