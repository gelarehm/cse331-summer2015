package notes.ch6dynamicprogramming.palindrome;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by jessehartloff on 7/23/15.
 */
public class PalindromeProblem {

    private String inputString; // I don't feel like messing with Strings
    private int[] inputValues;

    public PalindromeProblem(int n){
        inputValues = new int[n];
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            inputValues[i] = random.nextInt(26) + 1;
        }
    }

    public int solutionValue(int[] palindrome){
        int k = palindrome.length;
        if(k == 0){
            return 0;
        }
        if(!isPalindrome(palindrome)){
            return -1;
        }else{
            int value = 0;
            for (int i = k/2; i < k; i++) {
                value += palindrome[i];
            }
            return value;
        }

    }

    public boolean isPalindrome(int[] palindrome){
        int k = palindrome.length;
        if(k == 0){
            return true;
        }

        int pointer1;
        int pointer2;

        for(pointer1 = (k/2 + k%2 -1), pointer2 = k/2;
            pointer1 >= 0 && pointer2 < k && (palindrome[pointer1] == palindrome[pointer2]);
            pointer1--, pointer2++);

        return pointer1 == -1 && pointer2 == k;

    }

    public int[] getInput(){
        return inputValues;
    }

    @Override
    public String toString() {
        return Arrays.toString(inputValues);
    }
}
