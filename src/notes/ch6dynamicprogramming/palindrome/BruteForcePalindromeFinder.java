package notes.ch6dynamicprogramming.palindrome;

import java.util.Arrays;

/**
 * Created by jessehartloff on 7/23/15.
 */
public class BruteForcePalindromeFinder {

    public static int[] findBestPalindrome(PalindromeProblem problem){
        int[] bestPalindrome = new int[] {0};
        int bestValue = 0;

        int[] input = problem.getInput();
        int n = input.length;

        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                int[] toTest = Arrays.copyOfRange(input, i,j);
                int currentValue = problem.solutionValue(toTest);
                if(currentValue > bestValue){
                    bestValue = currentValue;
                    bestPalindrome = toTest;
                }
            }
        }

        return bestPalindrome;
    }


}
