package notes.ch6dynamicprogramming.shortestpathwithnegatives;

import notes.ch4greedy.shortestpath.DijkstrasAlgorithm;
import notes.ch4greedy.shortestpath.DijkstrasAlgorithmSlow;
import notes.ch4greedy.shortestpath.ShortestPathProblem;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class Main {

    public static void main(String[] args) {

        int n=10;
        ShortestPathWithNegativesProblem problem = new ShortestPathWithNegativesProblem(n);
        System.out.println(problem.getGraph());


        System.out.println();
        System.out.println("Dijkstra's Paths:");
        int[] dijkstraDistances = DijkstrasAlgorithmSlow.computeSolution(new ShortestPathProblem(problem.getGraph(), problem.getStartNode()));


        System.out.println();
        System.out.println("Bellman-Ford Paths:");
        int[] distances = BellmanFord.computeDistances(problem);

        System.out.println();
        System.out.println();
        System.out.println("Dijkstra's solution:");
        DijkstrasAlgorithm.printDistances(dijkstraDistances);

        System.out.println();
        System.out.println("Bellman-Ford solution:");
        DijkstrasAlgorithm.printDistances(distances);
    }

}
