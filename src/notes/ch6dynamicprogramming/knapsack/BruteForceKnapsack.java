package notes.ch6dynamicprogramming.knapsack;

import java.util.*;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class BruteForceKnapsack {

    public static Set<Item> generateSolution(KnapsackProblem problem){

        List<Item> items = new ArrayList<>(problem.getItems());
        int capacity = problem.getCapacity();

        Collections.sort(items, new Item.WeightComparator());
        Set<Item> packedItems = new HashSet<>(problem.getItems().size());

//        recursiveSolution(packedItems, items, 0, capacity);

//        boolean[] thingInTheThing = new boolean[items.size()];
//        anotherRecursion(thingInTheThing, items.size()-1, items, capacity);

        int n = problem.getItems().size();
        long numberOfSolutions = 1;
        
        for (int i = 0; i < n; i++) {
            numberOfSolutions <<= 1;
            numberOfSolutions += 1;
        }


        long bestSolution = 0;
        int bestValue = 0;

        for (int i = 0; i < numberOfSolutions; i++) {
            int currentWeight = 0;
            int currentValue = 0;
            for (int j = 0; j < n; j++) {
                long mask = 1 << j;
                if((i & mask) != 0){
                    currentWeight += items.get(j).getWeight();
                    currentValue += items.get(j).getValue();
                    if(currentWeight > capacity){
                        break;
                    }
                }
            }
            if(currentWeight > capacity){
                continue;
            }
            if(currentValue > bestValue){
                bestValue = currentValue;
                bestSolution = i;
            }
        }

        for (int j = 0; j < n; j++) {
            long mask = 1 << j;
            if((bestSolution & mask) != 0){
                packedItems.add(items.get(j));
            }
        }

        return packedItems;
    }

    private static int anotherRecursion(boolean[] thingsInTheThing, int recursionDepth, List<Item> items, int capacity){

        if(recursionDepth <= 0){
            int totalWeight = 0;
            for (int i = 0; i < thingsInTheThing.length; i++) {
                if(thingsInTheThing[i]) {
                    totalWeight += items.get(i).getWeight();
                }
            }
            return totalWeight > capacity ? 0 : totalWeight;
        }

        // in
        thingsInTheThing[recursionDepth] = true;
        recursionDepth--;
        int in = anotherRecursion(thingsInTheThing, recursionDepth, items, capacity);


        // out
        thingsInTheThing[recursionDepth] = false;
        recursionDepth--;
        int out = anotherRecursion(thingsInTheThing, recursionDepth, items, capacity);

        if(in > out){
            thingsInTheThing[recursionDepth] = true;
            return in;
        } else{
            thingsInTheThing[recursionDepth] = false;
            return out;
        }

    }

    private static int recursiveSolution(Set<Item> packedItems, List<Item> items, int currentWeight, int capacity){

        if(items.size() == 0){
            packedItems = new HashSet<>();
            return currentWeight;
        }

        // with
        int withWeight = 0;
        if(currentWeight + items.get(items.size()-1).getWeight() <= capacity) {
            withWeight = recursiveSolution(packedItems, items.subList(0, items.size() - 1),
                    currentWeight + items.get(items.size() - 1).getWeight(), capacity);
        }

        // without
        int withoutWeight = recursiveSolution(packedItems, items.subList(0, items.size() - 1),
                currentWeight, capacity);


        if(withWeight > withoutWeight){
//            packedItems = new HashSet<>(packedItems);
            packedItems.add(items.get(items.size() - 1));
            return currentWeight + items.get(items.size()-1).getWeight();
        } else{
//            packedItems = new HashSet<>(packedItems);
            packedItems.remove(items.get(items.size() - 1));
            return currentWeight;
        }

    }

}
