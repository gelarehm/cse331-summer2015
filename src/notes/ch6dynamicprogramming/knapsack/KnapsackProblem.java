package notes.ch6dynamicprogramming.knapsack;

import java.util.*;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class KnapsackProblem {

    private Set<Item> items;
    private int capacity;

    private static final int MIN_WEIGHT = 20;
    private static final int MAX_WEIGHT = 50;

    private static final int MIN_VALUE = 100;
    private static final int MAX_VALUE = 200;

    public KnapsackProblem(int n, int capacity){
        items = new HashSet<>(n);
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            int weight = random.nextInt(MAX_WEIGHT-MIN_WEIGHT+1) + MIN_WEIGHT;
            int value = random.nextInt(MAX_WEIGHT-MIN_WEIGHT+1) + MIN_WEIGHT;
            items.add(new Item(weight, value));
        }
        this.capacity = capacity;
    }

    public Set<Item> getItems(){
        return items;
    }

    public int getCapacity(){
        return capacity;
    }

    public static int checkWeight(Set<Item> solution){
        int totalWeight = 0;
        for(Item item : solution){
            totalWeight += item.getWeight();
        }
        return totalWeight;
    }

    public static int checkValue(Set<Item> solution){
        int totalValue = 0;
        for(Item item : solution){
            totalValue += item.getValue();
        }
        return totalValue;
    }

    @Override
    public String toString() {
        return "capacity = " + capacity +
                "\nitems=(" + items + ")";
    }
}
