package notes.ch5divideconquer.maximimumsubarray;

import javafx.util.Pair;

import java.util.List;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class StockTrader {

    public static Pair<Integer, Integer> computeBestTrade(MaximumSubarrayProblem problem){

        // Turns out this is faster than the divide an conquer solution

        List<Integer> prices = problem.getPrices();
        int n = prices.size();

        int purchaseTime = n-1;
        int sellTime = n-1;

        int maxPriceIndex = n-1;
        int maxPriceFound = Integer.MIN_VALUE;
        int maxProfitFound = Integer.MIN_VALUE;

        for (int i = n-1; i >= 0 ; i--) {
            int currentPrice = prices.get(i);
            if(currentPrice > maxPriceFound){
                maxPriceFound = currentPrice;
                maxPriceIndex = i;
            }
            if(maxPriceFound - currentPrice > maxProfitFound){
                purchaseTime = i;
                sellTime = maxPriceIndex;
                maxProfitFound = maxPriceFound - currentPrice;
            }
        }

        return new Pair<>(purchaseTime, sellTime);
    }

}
