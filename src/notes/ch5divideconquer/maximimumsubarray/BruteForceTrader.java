package notes.ch5divideconquer.maximimumsubarray;

import javafx.util.Pair;

import java.util.List;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class BruteForceTrader {

    public static Pair<Integer, Integer> computeBestTrade(MaximumSubarrayProblem problem){

        List<Integer> prices = problem.getPrices();
        int n = prices.size();

        int purchaseTime = 0;
        int sellTime = n-1;

        int bestProfit = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                if((prices.get(j) - prices.get(i)) > bestProfit){
                    purchaseTime = i;
                    sellTime = j;
                    bestProfit = prices.get(j) - prices.get(i);
                }
            }
        }

        return new Pair<>(purchaseTime, sellTime);
    }

}
